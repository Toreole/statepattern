﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerfReq_Pres
{
    public class StateMachine : MonoBehaviour
    {
        IState<StateMachine> activeState;

        private void Start()
        {
            activeState = new NormalState();
        }

        private void Update()
        {
            activeState.Update(this);
        }
    }
}