﻿namespace PerfReq_Pres
{
    public interface IState<T>
    {
        void Update(T machine);
        void EnterState(T machine);
        void ExitState(T machine);
    }

    public class NormalState : IState<StateMachine>
    {
        public void EnterState(StateMachine machine)
        {
            //...
        }

        public void ExitState(StateMachine machine)
        {
            //...
        }

        public void Update(StateMachine machine)
        {
            //...
        }
    }
}