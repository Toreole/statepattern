﻿using UnityEngine;
using System.Collections.Generic;

namespace PerfReq_Pres
{
    public class StackStateMachine : MonoBehaviour
    {
        Stack<IState<StackStateMachine>> stateStack;

        private void Start()
        {
            stateStack.Push(new NormalStateStack());
            stateStack.Peek().EnterState(this);
        }

        private void Update()
        {
            var activeState = stateStack.Peek();
            activeState.Update(this);
        }

        public void PushState(IState<StackStateMachine> state)
        {
            stateStack.Peek().ExitState(this);
            stateStack.Push(state);
            state.EnterState(this);
        }

        public void PopState()
        {
            stateStack.Pop().ExitState(this);
            stateStack.Peek().EnterState(this);
        }
    }

    public class NormalStateStack : IState<StackStateMachine>
    {
        public void EnterState(StackStateMachine machine)
        {
            //...
        }

        public void ExitState(StackStateMachine machine)
        {
            //...
        }

        public void Update(StackStateMachine machine)
        {
            //...
        }
    }
}
