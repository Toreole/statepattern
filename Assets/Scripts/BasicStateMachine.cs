﻿using UnityEngine;

namespace PerfReq_Pres
{
    public class BasicStateMachine : MonoBehaviour
    {
        public enum State
        {
            Idle, Running, Swimming
        }

        public State state;

        private void Update()
        {
            switch(state)
            {
                case State.Idle:
                    //Do Stuff
                    break;
                case State.Running:
                    //Do Stuff
                    break;
                case State.Swimming:
                    //Do Stuff
                    break;
            }
        }

    }
}